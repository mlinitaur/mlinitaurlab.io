const root = document.documentElement;

function updateColors() {
    var r = Math.floor((Math.random() * 230) + 1).toString();
    var g = Math.floor((Math.random() * 230) + 1).toString();
    var b = Math.floor((Math.random() * 230) + 1).toString();
    root.style.setProperty('--main', "rgba(" + r + "," + g + "," + b + ")");
}

root.addEventListener("mousedown", updateColors);

updateColors();
